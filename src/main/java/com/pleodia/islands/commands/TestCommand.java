package com.pleodia.islands.commands;

import net.minecraft.network.chat.TextComponent;
import tk.nuggetmc.craftnugget.cmd.CommandRegistry;
import tk.nuggetmc.nugget.command.Command;

import static net.minecraft.server.command.CommandManager.literal;

public class TestCommand extends Command {

    public TestCommand() {
        super("pleodia.commands.testcommand");
        CommandRegistry.INSTANCE.register((dispatcher -> dispatcher.register(
                literal("testcommand").requires(this::testPermission)
                .executes(context -> {
                    context.getSource().sendFeedback(new TextComponent("Command Success!"), false);
                    return 1;
                })
        )));
    }
}
