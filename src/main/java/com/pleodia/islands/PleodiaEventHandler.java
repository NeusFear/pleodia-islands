package com.pleodia.islands;


import tk.nuggetmc.nugget.ChatColor;
import tk.nuggetmc.nugget.entity.Player;
import tk.nuggetmc.nugget.event.EventHandler;
import tk.nuggetmc.nugget.event.events.player.PlayerLoginEvent;

public class PleodiaEventHandler {

    @EventHandler
    public void onJoin(PlayerLoginEvent event) {
        Player p = event.getPlayer();
        p.sendMessage(ChatColor.YELLOW + "Welcome " + p.getPlayerName() + "!");
    }
}
