package com.pleodia.islands;

import com.pleodia.islands.commands.TestCommand;
import net.fabricmc.api.ModInitializer;
import tk.nuggetmc.nugget.Nugget;
import tk.nuggetmc.nugget.event.EventHandler;
import tk.nuggetmc.nugget.event.EventManager;
import tk.nuggetmc.nugget.event.events.server.ServerReadyEvent;
import tk.nuggetmc.nugget.mod.Mod;

public class PleodiaIslands implements ModInitializer, Mod {

    @EventHandler
    public void onReady(ServerReadyEvent e) {
        Nugget.getLogger().info("Loaded" + getName() + "Plugin");
        Nugget.getLogger().info(getName() + " running Nugget version: " + Nugget.getNuggetVersion());

        new TestCommand();
    }

    @Override
    public void onInitialize() {
        EventManager.register(this);
        EventManager.register(new PleodiaEventHandler());
    }

    @Override
    public String getName() {
        return "PleodiaIslands";
    }

    @Override
    public void reload() {
        System.out.println("Reloading not enabled for this plugin.");
    }
}
